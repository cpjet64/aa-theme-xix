# Alliance Auth XIX Theme

## Installation

```shell
pip install git+https://gitlab.com/legion-of-xxdeathxx/aa-theme-xix.git
```

Now open your `local.py` and add the following right below your `INSTALLED_APPS`:
```python
# XIX Theme - https://gitlab.com/legion-of-xxdeathxx/aa-theme-xix
INSTALLED_APPS.insert(0, "aa_theme_xix")
```

After installation, run the command:
```shell
python manage.py collectstatic
```

**Important**

If you are using [aa-gdpr](https://gitlab.com/tactical-supremacy/aa-gdpr), the template stuff needs to be **after** the `aa-gdpr`
entry, like this:

```python
# GDPR Compliance
INSTALLED_APPS.insert(0, "aagdpr")
AVOID_CDN = True


# XIX Theme - https://gitlab.com/legion-of-xxdeathxx/aa-theme-xix
INSTALLED_APPS.insert(0, "aa_theme_xix")
```
